from flask import render_template, abort
from flask_login import login_required, current_user
from sqlalchemy import desc
from . import main
from colonize.models import Habitat, User, Role


@main.route('/dashboard')
@main.route('/')
@login_required
def dashboard():
    user_habitat = Habitat.query.filter_by(id_habitat=current_user.habitat_id).first()
    if user_habitat is None:
        return abort(500)

    data = {
        'user_habitat': user_habitat
    }
    return render_template('pages/dashboard.html', title='Domovská stránka', data=data)


@main.route('/habitat/<int:id_habitat>')
def habitat_overview(id_habitat: int):
    habitat = Habitat.query.filter_by(id_habitat=id_habitat).first()
    if habitat is None:
        return abort(404)

    inhabitants = User.query.filter_by(habitat_id=id_habitat).order_by(desc(User.role)).all()
    majordomus = User.query.filter_by(role=Role.MAJORDOMUS).first()

    data = {
        'habitat': habitat,
        'inhabitants': inhabitants,
        'number_of_inhabitants': len(inhabitants),
        'majordomus': majordomus
    }

    return render_template('pages/habitat_overview.html', title=f'Základna {data["habitat"].name} ', data=data)
