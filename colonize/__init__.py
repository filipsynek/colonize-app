from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

# login_manager = LoginManager()


app = Flask(__name__)
app.config.from_pyfile('settings.py')

""" 
Inicializace rozšíření aplikace
"""
db = SQLAlchemy(app)
login_manager = LoginManager(app)
login_manager.init_app(app)
login_manager.login_view = "auth.login_page"

"""
Načtení jednotlivých blueprintů, ze kterých se aplikace skládá
"""
from .main import main as main_blueprint

app.register_blueprint(main_blueprint)

from .auth import auth as auth_blueprint

app.register_blueprint(auth_blueprint)

from .error import ErrorPages

app.register_error_handler(403, ErrorPages.error_403)
app.register_error_handler(404, ErrorPages.error_404)
app.register_error_handler(500, ErrorPages.error_500)
