from flask import render_template, redirect, url_for, flash
from flask_login import login_user, logout_user, current_user
from werkzeug.security import check_password_hash
from . import auth
from colonize.forms import LoginForm
from colonize.models import User


@auth.route('/login', methods=['GET', 'POST'])
def login_page():
    form = LoginForm()

    if current_user.is_authenticated:
        """Přesměrování přihlášeného uživatele na dashboard"""
        return redirect(url_for('main.dashboard'))

    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        remember = form.remember.data

        user = User.query.filter_by(username=username).first()
        if user:
            """Uživatel se zadaným uživatelským existuje"""
            if check_password_hash(user.password_hash, password):
                """Zadané heslo se shoduje s hashem uloženým v databázi"""
                login_user(user, remember=remember)
                return redirect(url_for("main.dashboard"))
            else:
                """Zadané heslo a hash v databázi se neshodují"""
                print(f"Heslo pro uživatele {username} se neshoduje")
                flash("Nesprávné heslo")
        else:
            """Uživatel se zadaným uživatelským jménem neexistuje"""
            flash("Uživatel s tímto uživatelským jménem neexistuje")

    return render_template('pages/login.html', title='Přihlašovací stránka', form=form)


@auth.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("auth.login_page"))
