from flask import render_template


class ErrorPages:
    """ Třída definující chování stránek zobrazující chybové hlášky

    """

    def error_404(e):
        """ Chyba 404

        HTTP kód 404 - Stránka nenalezena

        :return: navrátí odkaz na šablonu se stránkou 404 a kód 404
        """
        return render_template("error/404.html", title="Stránka nenalezena"), 404

    def error_403(e):
        """ Chyba 404

        HTTP kód 403 - Zapovězený přístup na stránku

        :return: navrátí odkaz na šablonu se stránkou 403 a kód 403
        """

        return render_template("error/403.html", title="Přístup zapovězen"), 403

    def error_500(e):
        """ Chyba 500

        HTTP kód 403 - Chyba na straně serveru

        :return: navrátí odkaz na šablonu se stránkou 500 a kód 500
        """

        return render_template("error/500.html", title="Chyba serveru"), 500
