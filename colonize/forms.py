from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, length


class LoginForm(FlaskForm):
    username = StringField(label="Uživatelské jméno",
                           validators=[
                               DataRequired(message="Musíte zadat uživatelské jméno"),
                               length(min=3, max=64, message="Uživatelské jméno být dlouhé v rozmezí 3-64 znaků")
                           ])
    password = PasswordField(label="Heslo",
                             validators=[
                                 DataRequired(message="Musíte zadat heslo")
                             ])

    remember = BooleanField("Zapamatovat si přihlášení")
    submit = SubmitField("Přihlásit se")
