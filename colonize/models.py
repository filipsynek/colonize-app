import enum
from colonize import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    """ Funkce pro načtení přihlášeného uživatele

    Tato funkce načte uživatele z databáze podle předaného id

    :param int user_id: ID uživatele
    :return: Záznam uživatele
    """
    return User.query.filter_by(id_user=user_id).first()


class Gender(enum.Enum):
    """ Výčet pohlaví

    Třída uchovávající obě pohlaví, které lze uchovávat v databázi
    """
    MALE = 0
    FEMALE = 1


class Role(enum.Enum):
    """Výčet rolí

    Třída uchovávající možné role, které mohou uživatele mít
    """
    COLONIZER = 0
    MAJORDOMUS = 1
    MAJORPLANET = 2


class User(UserMixin, db.Model):
    """ Model tabulky `user`

    Třida definující model databázové tabulky user
    """
    __tablename__ = "user"
    id_user = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), nullable=False)
    password_hash = db.Column(db.String, nullable=False)
    first_name = db.Column(db.String(64), nullable=False)
    last_name = db.Column(db.String(64), nullable=False, unique=True)
    gender = db.Column(db.Enum(Gender), nullable=False)
    habitat_id = db.Column(db.Integer, db.ForeignKey('habitat.id_habitat'), nullable=False)
    role = db.Column(db.Enum(Role), nullable=False, default=Role.COLONIZER)

    def get_id(self) -> int:
        """ Funkce pro získání ID uživatele.

        Funkce je vyžadována modulem `flask_login` v případě, že primární klíč tabulky uživatele se jmenuje jinak než
        `ID`.

        :return: identifikační číslo uživatele

         """
        return int(self.id_user)

    def __init__(self, username: str, password_hash: str, first_name: str, last_name: str, gender: Gender,
                 habitat_id: int, role=Role.COLONIZER) -> None:
        """ Obecný konstruktor třídy `User`

        Pomocí tohoto konstruktoru lze nastavit všechny parametry, kromě ID, které si databázový systém nastavuje sám

        :param username: Přihlašovací jméno
        :param password_hash: Hash hesla
        :param first_name: Křestní jméno uživatele
        :param last_name: Příjmení uživatele
        :param gender: Pohlaví uživatele. Může nabývat hodnoty definované v enumeraci `Gender`
        :param habitat_id: Identifikace cizího klíče popisující habitat, ve kterém uživatel žije
        :param role: Role uživatele. Nabývá hodnot z enumerace Role. Výchozí hodnota je Role.COLONIZER
        """

        self.username = username
        self.password_hash = password_hash
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.habitat_id = habitat_id
        self.role = role

    def __repr__(self) -> str:
        return f"< User (id {self.id_user}), {self.username} -  {self.first_name} {self.last_name}, " \
               f"{self.gender} | {self.role}> @ Base ID:{self.habitat_id}>"


class Habitat(db.Model):
    """ Model tabulky 'habitat'

    Třída definující model tabulky `habitat` v databázi
    """
    __tablename__ = 'habitat'
    id_habitat = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, nullable=False)
    maximum_inhabitants = db.Column(db.Integer, nullable=False, default=32)
    x_position = db.Column(db.Float, nullable=False, default=0.0)
    y_position = db.Column(db.Float, nullable=False, default=0.0)
    inhabitants = db.relation("User", backref='habitat')

    def __repr__(self) -> str:
        return f"< Base (id {self.id_habitat}) {self.name} @ [{self.x_position}; {self.y_position}]>"

    def __init__(self, name: str, x_position: float, y_position: float, maximum_inhabitants=32) -> None:
        """ Obecný konstruktor třídy Habitat

        Pomocí tohoto konstruktoru lze nastavit všechny atributy třídy `Habitat`
        Atribut `id_habitat` je později nastaven databázovým systémem, do té doby má hodnotu `None`

        :param name: Jméno základny
        :param x_position: souřadnice X polohy základny
        :param y_position: souřadnice Y polohy základny
        :param maximum_inhabitants: Maximální počet obyvatel. Výchozí hodnota je 32
        """
        self.name = name
        self.maximum_inhabitants = maximum_inhabitants
        self.x_position = x_position
        self.y_position = y_position
