# Colonize App

## Rychlý popis

Aplikace pro udržování informací o kolonizátorech na planetě Proxima Centauri B, a základnách, které obývají.

## Informace o aplikace

* Aplikace je naprogramována v jazyku Python (verze 3.9.7)
* Jako databázový systém využívá SQLite
    * Pro komunikaci mezi aplikací a databází se využívá knihovna **SQLAlchemy**

## Spuštění aplikace (UNIX-based systémy)

### Před spuštěním

0. Ujistěte se, že máte v systému nainstalovaný python ve verzi 3.9. To lze zjistit pomocí příkazu `python --version`

1. Vytvořte si virtuální prostředí pomocí příkazu `python -m venv venv/` a aktivujte ho
   pomocí `source venv/bin/activate`

2. Po aktivaci virtálního prostředí si doinstalujte potřebné závislosti pro tuto aplikaci pomocí
   příkazu `pip install -r requirements.txt`

3. V adresáři `colonize/` vytvořte nový soubor s názvem `.env`, do kterého vložte proměnné prostředí uvedené v
   souboru `.env_example` a k nim příslušné hodnoty

### Spouštění

4. v terminálu definutje proměnné následující proměnné prostředí s uvedenými hodnotami:
    ```
    FLASK_APP=colonize
    FLASK_ENV=development
    ```

   Tento krok je nutné provést při každém novém sezení terminálu.

5. Ujistěte se, že jste v kořenovém adresáři projektu a spusťte lokální server pomocí příkazu `flask run`



